DOTFILES := $(shell pwd)
all: tmux shell vim2 repl conf vundle virtualenvwrapper

_ln:
ifneq ($(shell uname),Darwin)
	ln -sfv $(DOTFILES)/$(f) $(HOME)/$(t)
else
	ln -hsfv $(DOTFILES)/$(f) $(HOME)/$(t)
endif

tmux:
	make _ln f=tmux.conf t=.tmux.conf

shell:
	make _ln f=bin t=.bin
	make _ln f=bashrc t=.bashrc
	make _ln f=ackrc t=.ackrc
	make _ln f=zsh t=.zsh
	make _ln f=zshrc t=.zshrc
	make _ln f=inputrc t=.inputrc
	make _ln f=agignore t=.agignore

vim2:
	make _ln f=vim t=.vim
	make _ln f=vimrc t=.vimrc

repl:
	make _ln f=irbrc t=.irbrc
	make _ln f=pdbrc.py t=.pdbrc.py
	make _ln f=psqlrc t=.psqlrc

conf:
	make _ln f=config t=.config
	make _ln f=gitconfig t=.gitconfig
	make _ln f=pip t=.pip

vundle:
	git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
	git clone https://github.com/Valloric/YouCompleteMe ~/.vim/bundle/YouCompleteMe
	cd ~/.vim/bundle/YouCompleteMe ; git submodule update --init --recursive
	make vimproc
	make ycm
	@echo "vim -u ~/.dotfiles/vim/bundle.vim +BundleInstall +qa"

virtualenvwrapper:
	mkdir -p $(HOME)/.virtualenvs
	ln -svf ~/.dotfiles/virtualenvtools/post* $(HOME)/.virtualenvs

ycm:
	cd ~/.vim/bundle/YouCompleteMe ;  ./install.sh

vimproc:
	git clone https://github.com/Shougo/vimproc.vim ~/.vim/bundle/vimproc.vim
	cd ~/.vim/bundle/vimproc.vim ; make


vimup:
	@echo "vim -u ~/.dotfiles/vim/bundle.vim +BundleInstall +qa"
	vim -u ~/.dotfiles/vim/bundle.vim +BundleInstall +qa
