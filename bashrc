# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
# [ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups
export HISTIGNORE="exit"
export HISTSIZE=10000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
shopt -s histappend
PROMPT_COMMAND='history -a'

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    # eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'
fi

# some more ls aliases
alias ll='ls -lh'
alias la='ls -A'
alias lsd='ls -1ad */'
alias lk='ls -lSh'

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# set some colours for less
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'


# Define your own aliases here ...
#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi
alias nano='nano -w'
alias gksudo='gksudo -g'
alias grep='grep --color=auto'
alias pym='python manage.py'
alias i="sudo pacman -S"
alias u="sudo pacman -Sy"
alias uu="sudo pacman -Syu"
alias webshare='python -c "import SimpleHTTPServer;SimpleHTTPServer.test()"'
alias diefox='pgrep firefox | xargs kill -9'
alias fler="find . -type f -exec perl -pi -e 's/\r\n?/\n/g' {} \;"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

if [ -f $HOME/.git-completion.bash ]; then
    . $HOME/.git-completion.bash
fi

# django bash complete
if [ -f $HOME/python/django/trunk/extras/django_bash_completion ]; then
    . ~/python/django/trunk/extras/django_bash_completion
fi

PATH="$HOME/.bin:$PATH:/usr/local/bin:."
export PATH
PYTHONPATH=`python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()"`
PYTHONPATH="$PYTHONPATH:$HOME/python/site-packages:$HOME/python/django/plugins"
export PYTHONPATH=''
export SVN_MERGE='meld'


RED="\[\033[1;31m\]"
BLUE="\[\033[0;34m\]"
GREEN="\[\033[0;32m\]"
DEFAULT="\[\033[0;39m\]"

parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

# PS1='\n$BLUE\h \w $GREEN\$(__git_ps1 "(%s)")\n$DEFAULT> '
PS1="\n$BLUE\h \w $GREEN\$(parse_git_branch)\n$DEFAULT> "



# put this in your bashrc for bash tab completion with mpc
# $ cat mpc-bashrc >> ~/.bashrc

#export MPD_HOST="127.0.0.1"
#export MPD_PORT="6600"

 _mpdadd_complete_func ()
{
    cur="${COMP_WORDS[COMP_CWORD]}"
    first=${COMP_WORDS[1]}
    hold="";

    # add more escape stuff as needed:
    scrub='s/\([\\\>\\\<\\\(\\\)\\\";]\)/\\\1/g';

    case "$first" in
        add)
        hold=`mpc tab ${cur}`;
        COMPREPLY=($(compgen -W "${hold}" | sed "$scrub"))
        return 0
        ;;
        play|del|move)
        hold=`mpc playlist | sed 's/\#\([[:digit:]]\+\).*/\1/g'` # | grep "^${cur}"`
        COMPREPLY=($(compgen -W "${hold}" "${cur}"))
        return 0
        ;;
        # TODO add trailing 's' for seconds in seek (total song time needed)
        seek|volume)
        COMPREPLY=($(compgen -W "`seq 0 100; seq -f +%g 0 100; seq -f -%g 0 100`" "${cur}"))
        return 0
        ;;
        # TODO get total song time and use that as a limit
        crossfade)
        COMPREPLY=($(compgen -W "`seq 0 99`" "${cur}"))
        return 0
        ;;
        ls)
                if [ "x$cur" = "x" ]; then
                        hold=`mpc ls`;
                else
                hold=`mpc lstab ${cur}`;
                fi
        COMPREPLY=($(compgen -W "${hold}" | sed "$scrub"))
        return 0
        ;;
        search)
        COMPREPLY=($(compgen -W "album artist title filename" "${cur}"))
        return 0
        ;;
        load|save|rm)
                if [ "x$cur" = "x" ]; then
                        hold=`mpc lsplaylists`;
                else
                hold=`mpc loadtab ${cur}`;
                fi
        COMPREPLY=($(compgen -W "${hold}" | sed "$scrub"))
        return 0
        ;;
        repeat|random)
        COMPREPLY=($(compgen -W "0 1 true false yes no on off" "${cur}"))
        return 0
        ;;
        *)
        hold=`mpc help 2>&1 | grep "^mpc [a-z]\+ " | awk '{print $2}'`;
        COMPREPLY=($(compgen -W "${hold} status" ${cur}))
        return 0
        ;;
    esac
}
complete -F _mpdadd_complete_func mpc

alias pacs="pacsearch"
pacsearch () {
       echo -e "$(pacman -Ss $@ | sed \
       -e 's#current/.*#\\033[1;31m&\\033[0;37m#g' \
       -e 's#extra/.*#\\033[0;32m&\\033[0;37m#g' \
       -e 's#community/.*#\\033[1;35m&\\033[0;37m#g' \
       -e 's#^.*/.* [0-9].*#\\033[0;36m&\\033[0;37m#g' )"
}

alias np="newpython"
newpython(){

    if [ ! -n "$1" ]
    then
        echo "Usage: $((basename $0)) FILE"
    else
        echo "#!/usr/bin/env python" > $1
        chmod u+x $1
    fi
}
alias pg="pg"
pg(){
    ps aux | ack "$1" | ack -v ack
}

if [ "$TERM" = "linux" ]; then
echo -en "\e]P0222222" #black
echo -en "\e]P8222222" #darkgrey
echo -en "\e]P1803232" #darkred
echo -en "\e]P9982b2b" #red
echo -en "\e]P25b762f" #darkgreen
echo -en "\e]PA89b83f" #green
echo -en "\e]P3aa9943" #brown
echo -en "\e]PBefef60" #yellow
echo -en "\e]P46679a0" #darkblue
echo -en "\e]PC2b4f98" #blue
echo -en "\e]P5706c9a" #darkmagenta
echo -en "\e]PD826ab1" #magenta
echo -en "\e]P692b19e" #darkcyan
echo -en "\e]PEa1cdcd" #cyan
echo -en "\e]P7ffffff" #lightgrey
echo -en "\e]PFdedede" #white
clear #hmm, yeah we need this or else we get funky background collisions
fi

# Login greeting for screen------------------
if [ "$TERM" = "screen" -a ! "$SHOWED_SCREEN_MESSAGE" = "true" ]; then
  detached_screens=`screen -list | grep Detached`
  if [ ! -z "$detached_screens" ]; then
    echo "+---------------------------------------+"
    echo "| Detached screens are available:       |"
    echo "$detached_screens"
    echo "+---------------------------------------+"
  else
    echo "[ screen is activated ]"
  fi
  export SHOWED_SCREEN_MESSAGE="true"
fi

# set default look for open office
export OOO_FORCE_DESKTOP=gnome #&
export EDITOR=vim
# export PAGER=/usr/bin/most
LS_COLORS='no=00:fi=00:di=01;32:ln=01;33:pi=40;32:so=01;32:do=01;32:bd=40;33;01:cd=40;33;01:or=40;32;01:ex=01;31:*.tar=00:*.tgz=00:*.arj=00:*.taz=00:*.lzh=00:*.zip=00:*.z=00:*.Z=00:*.gz=00:*.bz2=00:*.deb=01;32:*.rpm=01;32:*.jpg=00;32:*.gif=00;32:*.bmp=00;32:*.ppm=00;32:*.tga=00;32:*.xbm=00;32:*.xpm=00;32:*.tif=00;32:*.png=00;32:*.mov=00;32:*.mp3=00;32:*.mpg=00;32:*.ogm=00;32:*.avi=00;32:*.fli=00;32:*.gl=01;32:*.dl=01;32:'; export LS_COLORS

# source $HOME/.bin/virtualenvwrapper_bashrc

# start screen if it's installed
# SCREEN=$(which screen 2>/dev/null)
# if [ ! -z $SCREEN ] ; then
    # screen -d -r || screen
    # clear
    # exit
# fi

