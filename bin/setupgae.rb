#!/usr/bin/env ruby
require 'open-uri'
require 'uri'


def main
  [
    "http://bitbucket.org/wkornewald/django-testapp/get/tip.zip",
    # "http://bitbucket.org/wkornewald/django-nonrel/get/tip.zip",
    # "http://bitbucket.org/wkornewald/djangoappengine/get/tip.zip",
    # "http://bitbucket.org/wkornewald/djangotoolbox/get/tip.zip",
    # "http://bitbucket.org/twanschik/django-autoload/get/tip.zip",
    # "http://bitbucket.org/wkornewald/django-dbindexer/get/tip.zip",
  ].each do |file|
    uri = URI.split(file)[5]
    fragments = uri.split('/').map {|x| x unless x.empty? }.compact
    dest = fragments[1]
    `wget #{file} && unzip -d tmp tip.zip && mv tmp/* #{dest} && rm -r tmp tip.zip`
  end
end

if __FILE__ == $0
  main
end
