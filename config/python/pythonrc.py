"""
This file is executed when the Python interactive shell is started if
$PYTHONSTARTUP is in your environment and points to this file. It's just
regular Python commands, so do what you will. Your ~/.inputrc file can greatly
complement this file.

"""
import os

try:
    import readline
    import atexit
except ImportError:
    print("You need readline, rlcompleter, and atexit")


try:
    import fancycompleter  # noqa
except ImportError:
    fancycompletion = False
    try:
        import rlcompleter  # noqa
    except ImportError:
        print("No completion available")
else:
    fancycompletion = True


# Make this work properly in Darwin and Linux
if 'libedit' in readline.__doc__:
    readline.parse_and_bind("bind ^I rl_complete")
else:
    readline.parse_and_bind("tab: complete")

WELCOME = ''


# Color Support
class TermColors(dict):
    """Gives easy access to ANSI color codes. Attempts to fall back to no color
    for certain TERM values. (Mostly stolen from IPython.)"""

    COLOR_TEMPLATES = (
        ("Black", "0;30"),
        ("Red", "0;31"),
        ("Green", "0;32"),
        ("Brown", "0;33"),
        ("Blue", "0;34"),
        ("Purple", "0;35"),
        ("Cyan", "0;36"),
        ("LightGray", "0;37"),
        ("DarkGray", "1;30"),
        ("LightRed", "1;31"),
        ("LightGreen", "1;32"),
        ("Yellow", "1;33"),
        ("LightBlue", "1;34"),
        ("LightPurple", "1;35"),
        ("LightCyan", "1;36"),
        ("White", "1;37"),
        ("Normal", "0"),
    )

    NoColor = ''
    _base = '\001\033[%sm\002'

    def __init__(self):
        if os.environ.get('TERM') in ('xterm-color', 'xterm-256color', 'linux',
                                      'screen', 'screen-256color',
                                      'screen-bce'):
            self.update(dict([(k, self._base % v) for k, v in self.COLOR_TEMPLATES]))
        else:
            self.update(dict([(k, self.NoColor) for k, v in self.COLOR_TEMPLATES]))
_c = TermColors()


import sys
# Enable Color Prompts
sys.ps1 = '%s>>> %s' % (_c['Green'], _c['Normal'])
sys.ps2 = '%s... %s' % (_c['Red'], _c['Normal'])


# Enable Pretty Printing for stdout
def my_displayhook(value):
    if value is not None:
        try:
            import __builtin__
            __builtin__._ = value
        except ImportError:
            __builtins__._ = value

        import pprint
        pprint.pprint(value)
        del pprint

sys.displayhook = my_displayhook


def which(object):
    """Print the source file from which a module, class, function, or method
    was imported.

    Usage:    >>> which(mysteryObject)
    Returns:  Tuple with (file_name, line_number) of source file, or None if
              no source file exists
    Alias:    whence
    """
    import types
    object_type = type(object)
    if object_type is types.ModuleType:
        if hasattr(object, '__file__'):
            print 'Module from', object.__file__
            return (object.__file__, 1)
        else:
            print 'Built-in module.'
    elif object_type is types.ClassType:
        if object.__module__ == '__main__':
            print 'Built-in class or class loaded from $PYTHONSTARTUP'
        else:
            print 'Class', object.__name__, 'from', \
                    sys.modules[object.__module__].__file__
            # Send you to the first line of the __init__ method
            return (sys.modules[object.__module__].__file__,
                    object.__init__.im_func.func_code.co_firstlineno)
    elif object_type in (types.BuiltinFunctionType, types.BuiltinMethodType):
        print "Built-in or extension function/method."
    elif object_type is types.FunctionType:
        print 'Function from', object.func_code.co_filename
        return (object.func_code.co_filename, object.func_code.co_firstlineno)
    elif object_type is types.MethodType:
        print 'Method of class', object.im_class.__name__, 'from',
        fname = sys.modules[object.im_class.__module__].__file__
        print fname
        return (fname, object.im_func.func_code.co_firstlineno)
    else:
        print "argument is not a module or function."
    del types
    return None


def view(fn):
    import inspect
    try:
        print(''.join(inspect.getsourcelines(fn)[0]))
    except TypeError as err:
        print('Cannot view source: %s' % err.args[0])
    del inspect


def edit(editor=None, *args, **kwargs):
    """
    Open a file in a preferred editor and execute it after the editor exits.

    `editor` specifies what editor to execute. The default is to infer it from
    the $EDITOR environment variable. Any additional positional arguments will
    be passed to the editor's process as argv.

    Keyword only arguments include:
        `content`: An initial set of lines to include in the created file
                   (default: None)
        `globals`: A dict containing the globals in which to execute the file
                   (default: globals())
        `write_globals`: A boolean specifying whether to write out a commented
                         out series of lines containing the globals to the
                         created file (useful for tab completion, default:
                         True)
        `retry`: Prompt to re-edit the file if an exception is raised on
                 execution (default: False)
        `add_history`: A callable that will be called for each line in the
                       edited file, that should add the line to the history, or
                       None to turn this feature off. (default:
                       `readline.add_history`)
        `filter_globals`: A function that will be used to filter the globals
                          when outputted. The default is to exclude any name
                          that starts with an underscore.

    """

    import os
    import subprocess
    import tempfile
    import traceback

    try:
        from readline import add_history
    except ImportError:
        add_history = kwargs.get("add_history")
    else:
        add_history = kwargs.get("add_history", add_history)

    editor = editor or os.environ.get("EDITOR")
    content = kwargs.get("content", "")
    globals_ = kwargs.get("globals", globals())
    write_globals = kwargs.get("write_globals", True)
    retry = kwargs.get("retry", False)
    matches = kwargs.get("filter_globals", lambda n: not n.startswith("_"))

    with tempfile.NamedTemporaryFile(delete=False, suffix=".py") as tmp:
        if content:
            tmp.writelines(content)
        if globals_ and write_globals:
            tmp.write("# Globals:\n")
            matching = (g for g in globals_ if matches(g))
            tmp.writelines("#    {}\n".format(g) for g in matching)

    while True:
        subprocess.call((editor, tmp.name,) + args)

        if add_history is not None:
            with open(tmp.name) as f:
                for line in f:
                    add_history(line.rstrip())

        try:
            execfile(tmp.name, globals_)
        except Exception:
            if retry:
                traceback.print_exc()
                answer = raw_input("Continue editing? [y]: ")
                if not answer or answer.lower() == "y":
                    continue
            raise

        break
    os.remove(tmp.name)


# Django Helpers
def SECRET_KEY():
    "Generates a new SECRET_KEY that can be used in a project settings file."

    from random import choice
    return ''.join([choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)')
                    for i in range(50)])

# If we're working with a Django project, set up the environment
print "Fix django setup..."
if False and 'DJANGO_SETTINGS_MODULE' in os.environ:
    from django.db.models.loading import get_models
    from django.test.client import Client
    from django.test.utils import setup_test_environment, teardown_test_environment  # noqa
    from django.conf import settings as S

    class DjangoModels(object):
        """Loop through all the models in INSTALLED_APPS and import them."""
        def __init__(self):
            for m in get_models():
                setattr(self, m.__name__, m)

    A = DjangoModels()
    C = Client()

    WELCOME += """%(Green)s
    Django environment detected.
* Your INSTALLED_APPS models are available as `A`.
* Your project settings are available as `S`.
* The Django test client is available as `C`.
%(Normal)s""" % _c

    setup_test_environment()
    S.DEBUG_PROPAGATE_EXCEPTIONS = True

    WELCOME += """%(LightPurple)s
Warning: the Django test environment has been set up; to restore the
normal environment call `teardown_test_environment()`.

Warning: DEBUG_PROPAGATE_EXCEPTIONS has been set to True.
%(Normal)s""" % _c

# Start an external editor with \e
# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/438813/

EDITOR = os.environ.get('EDITOR', 'vim')
EDIT_CMD = '\e'

from tempfile import mkstemp
from code import InteractiveConsole


class EditableBufferInteractiveConsole(InteractiveConsole):
    def __init__(self, locals=None, filename='<console>',
                 histfile=os.path.expanduser('~/.config/python/pyh')):
        self.last_buffer = []  # This holds the last executed statement
        InteractiveConsole.__init__(self, locals, filename)
        self.init_history(histfile)

    def init_history(self, histfile):
        readline.set_history_length(600)
        try:
            readline.read_history_file(histfile)
        except IOError:
            pass
        atexit.register(self.save_history, histfile)

    def save_history(self, histfile):
        readline.write_history_file(histfile)

    def runsource(self, source, *args):
        self.last_buffer = [source.encode('latin-1')]
        return InteractiveConsole.runsource(self, source, *args)

    def raw_input(self, *args):
        line = InteractiveConsole.raw_input(self, *args)
        if line == EDIT_CMD:
            fd, tmpfl = mkstemp('.py')
            os.write(fd, b'\n'.join(self.last_buffer))
            os.close(fd)
            os.system('%s %s' % (EDITOR, tmpfl))
            line = open(tmpfl).read()
            os.unlink(tmpfl)
            tmpfl = ''
            lines = line.split('\n')
            for i in range(len(lines) - 1):
                self.push(lines[i])
            line = lines[-1]
        if '??' in line:
            name = line.replace('??', '')
            from_globals = globals().get(name)
            if name is not None and from_globals is None:

                # perhaps we tried to get help on a literal
                from_globals = type(name)
            help(from_globals)
            return ''
        return line

# clean up namespace
del sys

# import some semi-useful libs if they're available
try:
    from see import see  # noqa
except ImportError:
    pass
try:
    from show import show  # noqa
except ImportError:
    pass


if __name__ == "__main__":
    c = EditableBufferInteractiveConsole(locals=locals())
    c.interact(banner=WELCOME)

    # Exit the Python shell on exiting the InteractiveConsole
    import sys
    sys.exit()
