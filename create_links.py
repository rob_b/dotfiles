#!/usr/bin/env python

import os
import shutil
from optparse import OptionParser


files = ("irbrc vimproject_mappings irbrc vim vimrc "
         "screenrc bashrc gitconfig").split()


def overwrite(src=False, dest=False):

    # if os.path.isdir(dest):
        # meth = os.removedirs
    # else:
    meth = os.remove
    try:
        meth(dest)
    except:
        raise


def backup(src=False, dest=False):

    src, dest = dest, dest + '.bak'
    try:
        if os.path.isdir(src):
            shutil.copytree(src, dest, symlinks=True)
        else:
            shutil.copy(src, dest)
    except:
        raise


def make_link(src=False, dest=False):

    try:
        os.symlink(src, dest)
    except OSError, e:
        if e.args[0] == 17:
            msg = analyse_file(src, dest)
    else:
        msg = "%s -> %s. Link created" % (src, dest)
    return msg


def analyse_file(src, dest):

    if os.path.islink(dest):
        link_dest = os.readlink(dest)
        if not os.path.samefile(src, link_dest):
            msg = "Symlink %s exists but points to the wrong file, %s"\
                % (dest, link_dest)
        else:
            msg = "Symlink %s already exists" % dest
    else:
        msg = "A file named %s already exists" % dest
    return msg


def main():

    usage = "usage: %prog [args]"
    parser = OptionParser(usage)
    options, args = parser.parse_args()
    actions = {'force': overwrite, 'keep': backup}

    if args:
        action = actions.get(args[0], False)
    else:
        action = None

    home = os.environ['HOME']
    for cfg in files:
        src = "%s/%s" % (os.getcwd(), cfg)
        dest = "%s/.%s" % (home, cfg)

        if callable(action):
            action(src=src, dest=dest)

        print make_link(src, dest)

if __name__ == "__main__":
    main()
