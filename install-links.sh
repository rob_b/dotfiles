#!/bin/bash

# Create needed directories if they don't already exist
mkdir -p "$HOME/.ssh"

FILES="
bashrc
bin
config
gitconfig
inputrc
jshintrc
pdbrc.py
pip
tmux.conf
vim
vimrc
zsh
zshrc
"

# Create symbolic links for all configuration files
for file in $FILES
do
    SOURCE="$HOME/.dotfiles/$file"
    TARGET="$HOME/.$file"

    # Create backup file if the target already exists and is not a symlink
    if [ -e "$TARGET" ] && [ ! -L "$TARGET" ]; then
        echo "*** WARNING *** $TARGET already exists; copying original to .$file.old"
        mv "$TARGET" "$TARGET.old"
    fi
    case $OSTYPE in
        darwin*)
            ln -hfsv "$SOURCE" "$TARGET"
            ;;
        linux*)
            ln -fsv "$SOURCE" "$TARGET"
            ;;
    esac
done

git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
echo "vim -u ~/.dotfiles/vim/bundle.vim +BundleInstall +qa"

mkdir -p "$HOME/.virtualenvs"
ln -sf ~/.dotfiles/virtualenvtools/post* "$HOME/.virtualenvs"

exit 0
