" Tab specific option
set tabstop=8     " A tab is 8 spaces
set expandtab     " Always uses spaces instead of tabs
set softtabstop=2 " Insert 2 spaces when tab is pressed
set shiftwidth=2  " An indent is 2 spaces
set smarttab      " Indent instead of tab at start of line
set shiftround    " Round spaces to nearest shiftwidth multiple
set nojoinspaces  " Don't convert spaces to tabs
set tw=99

" let g:ale_linters = {'haskell': ['hlint']}
" let g:ale_lint_on_save = 1
" hi! link ALEWarningSign GruvboxYellowSign
" hi! link ALEErrorSign GruvboxRedSign

setlocal equalprg=hindent

au FileType qf nnoremap <buffer> q :<C-u>cclose<CR>
au QuickFixCmdPost make call OpenQuickFixBuffer()

function! OpenQuickFixBuffer()
  let qflist = getqflist()
  if empty(qflist)
    cclose
  else
    copen
  endif
endfunction

setlocal includeexpr=substitute(v:fname,'\\.','/','g').'.hs'
setlocal suffixesadd=.hs,.lhs,.hsc,.hsx,hs,lhs,hsc,hsx


nnoremap <silent> <leader>o  :<C-u>CocList outline<cr>
