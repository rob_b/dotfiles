" let python_highlight_all = 1
" let python_print_as_function = 1
" let python_version_2 = 0
"
setlocal autoindent shiftwidth=4 tabstop=4 sta expandtab textwidth=99
nnoremap <buffer> <leader>ta :Pytest file<cr>
nnoremap <buffer> <leader>tf :Pytest function<cr>

if executable('yapf')
  set equalprg=yapf
endif

let g:ale_python_flake8_options = '--select I,E,F,W'
" let b:ale_fixers = {'python': ['black']}

let g:black_linelength = 99


function! IsortFn()
  execute ':silent !isort %'
endfunction

command! Isort call IsortFn()
