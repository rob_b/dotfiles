setlocal foldmethod=marker textwidth=0 nowrap

" always open help files in the top split
au BufWinEnter *.txt if &ft == 'help' | wincmd K | endif
