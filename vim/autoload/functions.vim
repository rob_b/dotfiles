function! functions#RestoreCursor()
    if line ("'\"") <= line("$")
      exe "normal! g`\""
      return 1
    endif
endfunction

function! functions#Sole_window()
    execute "tabonly"
    execute "only"
endfunction


" saves all the visible windows if needed/possible
function! functions#AutoSave()
  let this_window = winnr()

  windo call functions#SmartUpdate()

  execute this_window . 'wincmd w'

endfunction

function! functions#SmartUpdate()
  if &buftype != "nofile" && expand('%') != '' && &modified
    write
  endif
endfunction


function! functions#NormalizeWidths()
  let eadir_pref = &eadirection
  set eadirection=hor
  set equalalways! equalalways!
  let &eadirection = eadir_pref
endf


function! functions#SetGuiCursor()
  " mode aware cursors
  set guicursor=a:block-Cursor
  set guicursor+=o:hor50-Cursor
  set guicursor+=n:NormalCursor
  set guicursor+=i-ci-sm:ver30-InsertCursor
  set guicursor+=r-cr:ReplaceCursor-hor20
  set guicursor+=c:CommandCursor
  set guicursor+=v-ve:VisualCursor
  set guicursor+=a:blinkon0

  hi NormalCursor guifg=#444444 guibg=#5af527
  hi InsertCursor  ctermfg=15 guifg=#fdf6e3 ctermbg=37  guibg=#2aa198
  hi VisualCursor  ctermfg=15 guifg=#fdf6e3 ctermbg=125 guibg=#d33682
  hi ReplaceCursor ctermfg=15 guifg=#fdf6e3 ctermbg=65  guibg=#dc322f
  hi CommandCursor ctermfg=15 guifg=#fdf6e3 ctermbg=166 guibg=#cb4b16
endf

function! functions#blink(times, delay)
  let s:blink = { 'ticks': 2 * a:times, 'delay': a:delay }

  function! s:blink.tick(_)
    let self.ticks -= 1
    let active = self == s:blink && self.ticks > 0

    if !self.clear() && active && &hlsearch
      let [line, col] = [line('.'), col('.')]
      let w:blink_id = matchadd('IncSearch',
            \ printf('\%%%dl\%%>%dc\%%<%dc', line, max([0, col-2]), col+2))
    endif
    if active
      call timer_start(self.delay, self.tick)
    endif
  endfunction

  function! s:blink.clear()
    if exists('w:blink_id')
      call matchdelete(w:blink_id)
      unlet w:blink_id
      return 1
    endif
  endfunction

  call s:blink.clear()
  call s:blink.tick(0)
  return ''
endfunction


function! functions#MySearch()
  let grep_term = input("Enter search term: ")
  if !empty(grep_term)
    execute 'silent grep' grep_term | copen
  else
    echo "Empty search term"
  endif
  redraw!
endfunction
