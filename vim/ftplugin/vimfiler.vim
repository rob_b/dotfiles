setlocal colorcolumn=0 nolist nocursorline nowrap nonumber norelativenumber

function! s:vimfiler_settings()
  nmap <buffer> <C-l> :wincmd l<cr>
  nmap <buffer> <C-j> :wincmd j<cr>
  let g:vimfiler_ignore_pattern='.*\.egg-info,.*\.pyc$,.ropeproject,.DS_Store,__pycache__,.git'
endfunction
call s:vimfiler_settings()
