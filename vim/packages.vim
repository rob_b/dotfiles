call plug#begin('~/.vim/plugged')

Plug 'Lokaltog/vim-monotone'
Plug 'FelikZ/ctrlp-py-matcher'
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'christoomey/vim-tmux-navigator'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'davidhalter/jedi-vim'
Plug 'gregsexton/gitv'
Plug 'hdima/python-syntax'
Plug 'ivalkeen/vim-ctrlp-tjump'
Plug 'jeetsukumaran/vim-filebeagle'
Plug 'jmcantrell/vim-virtualenv'
Plug 'kana/vim-textobj-user'
Plug 'kshenoy/vim-signature'
" Plug 'maralla/completor.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'michaeljsmith/vim-indent-object'
Plug 'mitsuhiko/vim-python-combined'
Plug 'neovimhaskell/haskell-vim'
Plug 'othree/xml.vim'
Plug 'pangloss/vim-javascript'
Plug 'tacahiroy/ctrlp-funky'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'unblevable/quick-scope'
Plug 'dense-analysis/ale'
Plug 'wellle/targets.vim'
Plug 'wesQ3/vim-windowswap'
Plug 'vim-scripts/BufOnly.vim'
Plug 'alx741/vim-stylishask'
Plug 'andreypopp/vim-colors-plain'
Plug 'rakr/vim-one'
Plug 'rob-b/gruvbox'
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': './install.sh'
"     \ }
Plug 'psf/black'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'reasonml-editor/vim-reason-plus'

Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-ui'
call plug#end()

