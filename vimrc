" load our plugins  {{{

let g:ale_disable_lsp = 1
source ~/.vim/packages.vim

set viminfo='50,f1,:1000,/1000
set history=10000
set encoding=utf-8
set noerrorbells
set scrolloff=5
set noautochdir

" Detect filetype/syntax, enable plugins and indent rules
syntax on
filetype plugin on
filetype indent on
if (has("termguicolors"))
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" augroup qs_colors
"   autocmd!
"   autocmd ColorScheme * highlight QuickScopePrimary guifg='#FB007A' gui=underline ctermfg=9 cterm=underline
"   autocmd ColorScheme * highlight QuickScopeSecondary guifg='#005F87' gui=underline ctermfg=18 cterm=underline
" augroup END


if has("gui_running")

  " let g:monotone_color = [255, 100, 100] " Sets theme color to bright green
  " let g:monotone_secondary_hue_offset = 200 " Offset secondary colors by 200 degrees
  " let g:monotone_emphasize_comments = 0 " Emphasize comments
  " colorscheme monotone

  let g:gruvbox_italic = 1
  let g:gruvbox_hls_cursor = 'bright_aqua'
  let g:gruvbox_sign_column = 'light0'
  let g:gruvbox_vert_split = 'light0'
  set background=light
  colorscheme plain

  set linespace=2
  set macligatures
else
  set background=dark
  let g:gruvbox_sign_column = 'dark0'
  let g:gruvbox_vert_split = 'dark0'
  let g:gruvbox_hls_cursor = 'bright_aqua'
  colorscheme gruvbox
endif

" Store temporary files in a central spot
set backupdir=~/.vim/tmp
set directory=~/.vim/tmp
set noswapfile

" ignore whitespace when diffing
set diffopt+=iwhite

" Automatically reload .vimrc when changing
augroup ft_vimrc
    autocmd!
    autocmd bufwritepost .vimrc nested source %
    " Resave after losing focus
    " au FocusLost * call functions#AutoSave()

    " resize splits after window is resized
    " au VimResized * exe "normal! \<c-w>="

    " restart where we were last
    autocmd BufWinEnter * call functions#RestoreCursor()
augroup END

set autoread

" persistent undo if available
if has("persistent_undo")
    set undodir=~/.vim/tmp/undo
    set undofile

    if !isdirectory(expand(&undodir))
        call mkdir(expand(&undodir), "p")
    endif
endif

" reduce pause after using O to insert a line
set timeout
set ttimeout
set ttimeoutlen=100
set timeoutlen=400
set lazyredraw
set formatoptions+=lj
set nojoinspaces

set clipboard+=unnamed

" }}}

" file presentation {{{

" don't force # to column 0
" inoremap # X<BS>#
set backspace=indent,eol,start
set wrap
set showbreak=↪\  " Character to precede line wraps
set breakindent
set splitbelow
set splitright

set tabstop=2
set shiftwidth=2
set expandtab
set softtabstop=2
" set winwidth=80
set textwidth=78
set colorcolumn=""

" display tabs and trailing spaces
set list
set listchars=tab:·\ ,trail:•,eol:¬
set fillchars=vert:│,fold:-

" }}}

" vim presentation {{{

" no bell + less chrome when in gvim

if has("gui_running")
    set vb
    set t_vb=
    set guioptions-=T
    set guioptions-=e
    set guioptions-=r
    set guioptions-=L
    set guioptions+=c
    if has("macunix")
        " when in macvim go fullscreen
        " set fuoptions=maxvert,maxhorz
        " au GUIEnter * set fullscreen
        " set guifont=M+\ 1m\ regular:h13
        set guifont=Iosevka-Fixed-SS08:h14
        " set guifont=Pragmata\ Pro:h13
    else
        set gfn=Andale\ Mono\ 10
    endif
endif

" Use a bar-shaped cursor for insert mode, even through tmux.
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" set number
" set relativenumber

" briefly jump to the matching bracket when entering the closing pair
set showmatch

" statusline always on
set laststatus=2
" set ruler
set noshowmode

" Use incremental searching
set incsearch
set ignorecase
set smartcase

" Do highlight search results
set hlsearch

" use normal regex syntax
nnoremap / /\v
vnoremap / /\v

" global substitutions are more useful so do them by default
set gdefault

" tab complete menu
set wildcharm=<C-z>
set wildmenu
set wildmode=longest,full
set wildignore+=*.pyc,collected-static*,.git*,.DS_Store,_generated_media*,__pycache__,build

" set behaviour for onmicomplete menu
" set completeopt=longest,menuone
set completeopt=menuone,noinsert

" }}}

" folding {{{
set nofoldenable
" set foldmethod=indent
" set foldlevel=99

fu! CustomFoldText() " {{{
     "get first non-blank line
     let fs = v:foldstart
     while getline(fs) =~ '^\s*$' | let fs = nextnonblank(fs + 1)
     endwhile
     if fs > v:foldend
         let line = getline(v:foldstart)
     else
         let line = substitute(getline(fs), '\t', repeat(' ', &tabstop), 'g')
     endif

     let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
     let foldSize = 1 + v:foldend - v:foldstart
     let foldSizeStr = " " . foldSize . " lines "
     let foldLevelStr = repeat("+--", v:foldlevel)
     let lineCount = line("$")
     let foldPercentage = printf("[%.1f", (foldSize*1.0)/lineCount*100) . "%] "
     let expansionString = repeat(".", w - strwidth(foldSizeStr.line.foldLevelStr.foldPercentage))
     return line . expansionString . foldSizeStr . foldPercentage . foldLevelStr
endf
" }}}
set foldtext=CustomFoldText()

" Autoclose folds, when moving out of them
" set foldclose=all
" }}}

" autogroups {{{

" Always turn off paste when exiting Insert mode {{{
augroup NormalMode
    au!
    au InsertLeave * set nopaste
augroup END
" }}}


" Set cursorline only in active window {{{
" augroup CursorLineOnlyInActiveWindow
"   autocmd!
"   autocmd VimEnter,WinEnter,BufWinEnter * setlocal cursorline
"   autocmd WinLeave * setlocal nocursorline
" augroup END

" }}}


" mail {{{
augroup ft_mail
    au!
    au FileType mail setlocal tw=70 et
    au BufNewFile,BufRead *mutt-* exec '/^$'
augroup END
" }}}

" fugitive {{{
augroup fugitive_extra
    autocmd!
    autocmd BufReadPost fugitive://* set bufhidden=delete
augroup END
" }}}

" gina {{{
augroup gina_extra
    autocmd!
    autocmd BufReadPost gina://* set bufhidden=delete
augroup END
" }}}

" spellcheck please {{{
augroup with_spell
    autocmd!
    au FileType mail setlocal spell spelllang=en_gb
    au FileType rst setlocal spell spelllang=en_gb
    au FileType markdown setlocal spell spelllang=en_gb
    au FileType gitcommit setlocal spell spelllang=en_gb
augroup END
" }}}

au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
" }}}

" mappings {{{

" remap leader because "\" is seldom in the same place on different keyboards
let mapleader ="\<Space>"

" I don't really use visual mode
nnoremap Q <nop>

" close all but the current fold
nnoremap <leader>z zMzvzz
nnoremap zC [zV]zzC

" if surround.vim is installed, wrap a string in gettext function
vmap si S)i_<esc>f)hxp

" buffer
nnoremap <leader>ls :ls<cr>:buffer<space>

" unhighlight search results
noremap <silent> <Leader><Leader> :noh<CR><Space>

" save current file as root
cnoremap w!! w !sudo tee % > /dev/null

" make :W the same as :w
command! -bang -complete=file W w<bang> <args>
command! -bang -complete=file Wq wq<bang> <args>
command! -bang -complete=file WQ wq<bang> <args>
command! -bang -complete=file WQa wqa<bang> <args>
command! -bang -complete=file WQA wqa<bang> <args>
command! -bang -complete=file WQa wqa<bang> <args>
command! -bang -complete=file E e<bang> <args>

" Insert the directory of the current buffer in command line mode
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" Don't move the cursor when highlighting
noremap <silent>* *N:exec functions#blink(8, 20)<cr>
noremap <silent># #Nzz:exec functions#blink(8, 20)<cr>
noremap <silent>n nzz:exec functions#blink(8, 20)<cr>
noremap <silent>N Nzz:exec functions#blink(8, 20)<cr>

" make j & k work on wrapped lines
nnoremap j gj
nnoremap k gk
nnoremap <expr> k (v:count ? 'k' : 'gk')
nnoremap <expr> j (v:count ? 'j' : 'gj')


" clean whitespace only lines
map <silent> <leader>cel mz:%s:\s\+$::<CR>'z:delmarks z<CR>

" search for the word under the cursor in all files
" in this directory and below
if executable('ag')
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  set grepprg=ag\ --vimgrep
  set grepformat^=%f:%l:%c:%m   " file:line:column:message
  command! -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
endif

" tab bindings
" map <C-t> <Esc>:tabe<CR>
map gb <Esc>:tabprevious<CR>

" easier omnicomplete binding
inoremap <Nul> <C-x><C-o>

" leave insert mode
imap ;; <Esc>
inoremap jj <esc>

" shortcut to paste from clipboard
" map <C-p> "+gp

" toggle paste
nnoremap <silent><f2> :set invpaste paste?<cr>
set pastetoggle=<f2>

inoremap <Up> <NOP>
inoremap <Down> <NOP>
inoremap <Left> <NOP>
inoremap <Right> <NOP>
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
noremap <silent><C-k> :wincmd k<CR>
noremap <silent><C-j> :wincmd j<CR>
noremap <silent><C-h> :wincmd h<CR>
noremap <silent><C-l> :wincmd l<CR>

nnoremap <leader>todo :noautocmd vimgrep /TODO/ %<CR>
nnoremap <leader>fixme :noautocmd vimgrep /FIXME/ %<CR>
command! Todo noautocmd vimgrep /TODO\|FIXME/j ** | cw

nnoremap <C-e> :e#<cr>


fu! OpenInSplitIfBufferDirty(file) " {{{
    if line('$') == 1 && getline(1) == ''
        exec 'e' a:file
    else
        exec 'vsplit' a:file
    endif
endfu
" }}}

" open vimrc on the right
nnoremap <leader>ev :call OpenInSplitIfBufferDirty($MYVIMRC)<cr>
nnoremap <leader>ez :call OpenInSplitIfBufferDirty("$HOME/.zshrc")<cr>
nnoremap <leader>eb :call OpenInSplitIfBufferDirty("$HOME/.vim/bundle.vim")<cr>

" match indent of next line to start of current word
nnoremap <Leader><Tab> yiwy0o<esc>""pVr J

" create new lines simply when in normal mode
nnoremap <CR> o<ESC>

" highlight conflict markers
nnoremap <silent> <leader>cf <ESC>/\v^[<=>]{7}( .*\|$)<CR>

noremap <leader>high :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">" . " FG:" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"fg#")<CR>

command! Sole call functions#Sole_window()
command! Search call functions#MySearch()
nnoremap <leader>s :Search<cr>
nnoremap <leader>. :silent grep <cword> \| copen<CR><C-l>

" Disable one diff window during a three-way diff allowing you to cut out the
" noise of a three-way diff and focus on just the changes between two versions
" at a time. Inspired by Steve Losh's Splice
function! DiffToggle(window) " {{{
  " Save the cursor position and turn on diff for all windows
  let l:save_cursor = getpos('.')
  windo :diffthis
  " Turn off diff for the specified window (but keep scrollbind) and move
  " the cursor to the left-most diff window
  exe a:window . "wincmd w"
  diffoff
  set scrollbind
  set cursorbind
  exe a:window . "wincmd " . (a:window == 1 ? "l" : "h")
  exe a:window . "wincmd ="
  " Update the diff and restore the cursor position
  diffupdate
  call setpos('.', l:save_cursor)
endfunction
" }}}

" Toggle diff view on the left, center, or right windows
nmap <silent> <leader>dl :call DiffToggle(1)<cr>
nmap <silent> <leader>dc :call DiffToggle(2)<cr>
nmap <silent> <leader>dr :call DiffToggle(3)<cr>

" Re-select text block when shifting
vnoremap < <gv
vnoremap > >gv

" }}}

" plugin settings {{{

" coc.vim
let g:coc_node_path="/Users/rob/.nvm/versions/node/v10.16.3/bin/node"

" ctrlp
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
" let g:ctrlp_map = '<leader>p'
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_mruf_relative = 0
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:20,results:30'
let g:ctrlp_use_caching = 0
let g:ctrlp_prompt_mappings = {
    \ 'PrtClearCache()':      ['%'],
    \ }
" nnoremap <silent><leader>l :CtrlPLine<enter>
" nnoremap <silent><leader>b :CtrlPBuffer<enter>
nnoremap <silent><leader>o :CtrlPFunky<enter>
" nnoremap <silent><leader>m :CtrlPMRUFiles<enter>
" nnoremap <silent><leader>t :CtrlPTag<enter>
" nnoremap <silent><leader>d :CtrlPDir<enter>

" fzf
nnoremap <silent><leader>p :Files<enter>
nnoremap <silent><leader>l :Lines<enter>
nnoremap <silent><leader>b :Buffers<enter>
nnoremap <silent><leader>o :CtrlPFunky<enter>
nnoremap <silent><leader>m :History<enter>
nnoremap <silent><leader>t :Tags<enter>
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }

" ruby.vim
let ruby_space_errors = 1

" gitv
let Gitv_DoNotMapCtrlKey = 1

" gutentags
" let g:gutentags_cache_dir="~/.vim/tmp/tags"
let g:gutentags_ctags_exclude = ["Session.vim"]
let g:gutentags_project_info = [ {'type': 'python', 'file': 'setup.py'},
                               \ {'type': 'ruby', 'file': 'Gemfile'},
                               \ {'type': 'haskell', 'file': 'Setup.hs'} ]


let g:gutentags_ctags_executable_haskell = 'gutenhasktags'
let g:gutentags_define_advanced_commands=1
let g:gutentags_trace = 0
let g:gutentags_exclude_project_root = ['/usr/local/', '~/Projects', '~/']
let g:gutentags_generate_on_missing=0

" let g:ale_statusline_format = [' ⚑ %d', ' ⚐ %d', '']
" let g:ale_sign_column_always = 1

let g:ale_fixers = {'python': ['isort', 'black'], 'reason': ['refmt'], 'javascript': ['']}
let g:ale_fix_on_save = 1
let g:ale_python_auto_pipenv = 1
let g:go_fmt_command = "goimports"
let g:ale_set_highlights=0
let g:ale_linters = {'haskell': []}
" let g:ale_linters = {'haskell': ['hlint']}
" let g:ale_lint_on_save = 1

" mu complete
" set shortmess+=c
" set completeopt-=preview
" set completeopt+=noinsert,noselect
" let g:mucomplete#enable_auto_at_startup = 1

" completor.vim
" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr> pumvisible() ? "\<C-y>\<cr>" : "\<cr>"

" jedi
" let g:jedi#popup_on_dot = 0
" let g:jedi#smart_auto_mappings = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#auto_initialization = 0
let g:jedi#force_py_version = 3

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" context.vim
let g:context_filetype_blacklist = ['haskell']


" highlighting {{{

" taglist
hi MyTagListFileName guibg=NONE guifg=green

" Highlight VCS conflict markers
match ErrorMsg '^\(<\|=\|>\)\{7\}\([^=].\+\)\?$'
" }}}

" abbreviations {{{

ab bl <Esc>bi{% block <Esc>ea %}{% endblock %}<Esc>h%i
iab improt import
iab realted related
" }}}

" python things {{{

" execute current python file
map <F12> <Esc>:!/usr/bin/env python %<CR>

" python debug
noremap <leader>pdb Oimport pdb; pdb.set_trace()<ESC>^
inoremap <leader>pdb import pdb; pdb.set_trace()

" map <silent> <leader>ex isys.exit(1)<ESC>
" imap <silent> <leader>ex sys.exit(1)

" set tags+=$HOME/.vim/tags/python.ctags

" Add the virtualenv's site-packages to vim path
" py << EOF
" import os.path
" import sys
" import vim
" if 'VIRTUAL_ENV' in os.environ:
"     project_base_dir = os.environ['VIRTUAL_ENV']
"     sys.path.insert(0, project_base_dir)
"     activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"     execfile(activate_this, dict(__file__=activate_this))
" EOF
" }}}

" Status Line: {{{

" Status Function: {{{2

function! LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))

    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors

    return l:counts.total == 0 ? '' : printf(
    \   ' ⚐ %d ⚑ %d',
    \   all_non_errors,
    \   all_errors
    \)
endfunction
" let stat .= Color(active, 1, col(".") / 100 >= 1 ? '%02l:%v ' : ' %02l:%02v ')

function! GetErrors() " {{{
    return LinterStatus()
endfunction " }}}



function! SyntaxItem()
  return synIDattr(synID(line("."),col("."),1),"name")
endfunction

function! Enc()
    let encoding = strlen(&fenc) ? &fenc : &enc
    return encoding == 'utf-8' ? '' : encoding
endfunction

function! Status(winnr)
  let stat = ''
  let active = winnr() == a:winnr
  let buffer = winbufnr(a:winnr)

  let modified = getbufvar(buffer, '&modified')
  let readonly = getbufvar(buffer, '&ro')
  let fname = bufname(buffer)

  function! Color(active, num, content)
    if a:active
      return '%' . a:num . '*' . a:content . '%*'
    else
      return a:content
    endif
  endfunction

  " column
  let stat .= Color(active, 1, col(".") / 100 >= 1 ? '%02l:%v ' : ' %02l:%02v ')

  " file
  " let stat .= Color(active, 4, active ? ' »' : ' «')
  let stat .= Color(active, 6, ' %f ')
  " let stat .= ' ' . Color(active, 4, active ? '«' : '»')

  " git branch
  if active && exists('*fugitive#head')
    let head = fugitive#head()

    if empty(head) && exists('FugitiveDetect') && !exists('b:git_dir')
      call FugitiveDetect(getcwd())
      let head = fugitive#head()
    endif
    if !empty(head)
      let stat .= Color(active, 4, ' ➲ ' . head . ' ')
    endif
  endif


  " file modified
  let stat .= Color(active, 2, modified ? ' + ' : '')

  " read only
  let stat .= Color(active, 2, readonly ? ' ‼ ' : '')

  " paste
  if active && &paste
    let stat .= '%2*' . ' P ' . '%*'
  endif

  " what's the current highlight group
  " let stat .= " %{SyntaxItem()}"
  " let stat .= " %{ObsessionStatus()}"
  let tagStatus = "BROKEN"
  if exists('*gutentags#statusline')
    let tagStatus = gutentags#statusline("🔒")
  else
    let tagStatus = "⚰"
  endif
  if !empty(tagStatus)
    let stat .=" " . tagStatus . ' '
  endif


  " right side
  let stat .= '%='

  let stat .= Color(active, 5, "%{GetErrors()}")
  let stat .= "%{Enc()}"

  return stat
endfunction
" }}}

" Status AutoCMD: {{{
function! SetStatus()
  for nr in range(1, winnr('$'))
    call setwinvar(nr, '&statusline', '%!Status('.nr.')')
  endfor
endfunction

augroup status
  autocmd!
   autocmd VimEnter,WinEnter,BufWinEnter,BufUnload * call SetStatus()
augroup END
" }}}

" Status Colors: {{{

" 245 = #8a8a8a
" 22 = #005f00
" 26 = #005fd7
" 28 = #008700
" 156 = #afff87
" hi StatusLineNC guifg=#1c1c1c
hi StatusLine cterm=NONE
hi TabLineSel guibg=#bd7589 guifg=#e9d1d7

hi User1 ctermbg=28 ctermfg=156 guibg=#007300 guifg=#afff87
hi User6 guibg=#008700 guifg=#afff87 ctermbg=36 ctermfg=194
hi User4 ctermbg=22 ctermfg=156 guifg=#afff87 guibg=#40A540

hi User2 guibg=#df5970 guifg=#d7d700 ctermfg=184 ctermbg=124
hi User3 ctermbg=28 ctermfg=156 guifg=#afff87 guibg=#008700
hi User5 ctermbg=88 ctermfg=231 guifg=#ffffff guibg=#870000

function! UserHi()
  exec "hi User1 ctermbg=28 ctermfg=156 guibg=#007300 guifg=#afff87"
  exec "hi User6 guibg=#008700 guifg=#afff87 ctermbg=36 ctermfg=194"
  exec "hi User4 ctermbg=22 ctermfg=156 guifg=#afff87 guibg=#40A540"
  " exec "hi CursorLine guibg=#2b3325"
endfunction


function! UserHiInvert()
  exec "hi User1 ctermbg=28 ctermfg=156 guibg=#0051D9 guifg=#afffff"
  exec "hi User6 guibg=#005FFF guifg=#afffff ctermbg=31"
  exec "hi User4 ctermbg=22 ctermfg=156 guifg=#afff87 guibg=#4087FF"
  " exec "hi CursorLine guibg=#252e33"
endfunction

augroup hi_statusline
  autocmd!
  au InsertEnter * call UserHiInvert()
  au InsertLeave,BufRead,BufNewFile * call UserHi()
augroup END

augroup Cursors
  " au InsertEnter * call functions#SetGuiCursor()
  " au VimEnter * call functions#SetGuiCursor()
augroup END

" }}}

" }}}
