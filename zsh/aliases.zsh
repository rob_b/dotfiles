if [ -e /usr/local/bin/brew -a -e /usr/local/opt/coreutils/libexec/gnubin ]; then
    prefix=/usr/local/opt/coreutils/libexec/gnubin
    alias ls="$prefix/ls -h --color=auto"
    alias dircolors=$prefix/dircolors
    alias wc=$prefix/wc
fi
if [ -e /usr/local/bin/gawk ]; then
    alias awk='/usr/local/bin/gawk'
fi

case $OSTYPE in
    linux*)
        alias ls='ls -h --color=auto'
        alias xargs='xargs -r'
        ;;

    freebsd*)
        alias ls='ls -Gh'
        alias pkg_delete='pkg_delete -drv'
        alias portaudit-get='portaudit -Fd'
        alias portupdate='cvsup -L 2 /etc/ports-supfile'
        alias poweroff='shutdown -p now'
        ;;
esac

alias .='source'

alias df='df -h'
alias du='du -h'
alias head='head -n20'
alias la='ls -ah'
alias ll='ls -lh'
alias lla='ls -lha'
alias lsd='ls -d */'
# alias tail='tail -n20'
alias pym='python manage.py'
alias webshare='python -m SimpleHTTPServer'
alias diefox='pgrep firefox | xargs kill -9'
alias fler="find . -type f -exec perl -pi -e 's/\r\n?/\n/g' {} \;"
alias np="newpython"
alias oi="sudo"
alias screen="deactivate 2>/dev/null; screen"
alias tmux="tmux -2"
if [ -e /Applications/MacVim.app/Contents/MacOS/Vim ]; then
    alias vim=/Applications/MacVim.app/Contents/MacOS/Vim
    alias mvim=/Applications/MacVim.app/Contents/bin/mvim
    # export EDITOR=vim
    export EDITOR=/Applications/MacVim.app/Contents/MacOS/Vim
    export GIT_EDITOR=/Applications/MacVim.app/Contents/MacOS/Vim
fi

alias -g ...='../..'
alias -g ....='../../..'
alias -g G='| grep'
alias -g H='| head'
alias -g L='| less'
alias -g N='> /dev/null'
alias -g M='| more'
alias -g T='| tail'
alias -g X='| xargs'

alias -s avi='mplayer'
alias -s c='vim'
alias -s h='vim'
alias -s htm='elinks'
alias -s html='elinks'
alias -s jpg='eog'
alias -s jpeg='eog'
alias -s mpg='mplayer'
alias -s png='eog'
# alias -s py='vim'
alias -s txt='vim'
alias -s wmv='mplayer'

# quickly change to some common directories
alias dotfiles="cd ~/.dotfiles"

# if we're on ubuntu (maybe all debian derivatives) ack is installed as ack-grep
[[ -e /usr/bin/ack-grep ]] && alias ack='ack-grep'
alias ag='ag --pager less --smart-case'

# ruby bundler
alias b='bundle'
alias be='b exec'
