setopt prompt_subst

parse_git_branch() {
    git branch 2> /dev/null | sed -e "/^[^*]/d" -e "s/* \(.*\)/[\1${red}$(git_status)${green}]/"
}
git_status(){
    if current=$(git status 2>/dev/null | grep 'added to commit' 2> /dev/null); then
        echo "☠"
    fi
}
hg_status(){
    st=""
    if mod=$(hg status 2>/dev/null | grep '^M' 2>/dev/null); then
         st="${yellow}⊛${nocolor} "
    fi
    if add=$(hg status 2>/dev/null | grep '^A' 2>/dev/null); then
        st=$st"${green}⊕${nocolor}"
    fi
    echo $st
}

# Show remote ref name and number of commits ahead-of or behind
function +vi-git-st() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    br=''
    br+="${white}${hook_com[branch]}${nocolor}"
    br+="%F{240}@%f%F{244}${hook_com[revision][1,7]}%f"

    if [[ -n ${remote} ]] ; then
        # for git prior to 1.7
        # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
        ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
        (( $ahead )) && gitstatus+=( "${green}+${ahead}${nocolor}" )

        # for git prior to 1.7
        # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
        behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
        (( $behind )) && gitstatus+=( "${lightred}-${behind}${nocolor}" )

        if [[ ${#gitstatus} -gt 0 ]]; then
            position=${(j:/:)gitstatus}
            position=" "${position}
        else
            position=''
        fi
        upstream=""
        br+=" ${gray}[${upstream}${remote}${position}]${nocolor}"
    fi

    if git ls-files --other --exclude-standard --directory --no-empty-directory 2> /dev/null | grep -q "."; then
        br+="%F{161}${CHANGE_SYMBOL}%f"
    fi

    hook_com[branch]="${br}"
}


# Show count of stashed changes
function +vi-git-stash() {
    local -a stashes

    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        stashes=$(git stash list 2>/dev/null | wc -l)
        hook_com[misc]+=" (${stashes} stashed)"
    fi
}

venv(){
    [[ -n $VIRTUAL_ENV ]] && echo "%F{228}$(basename $VIRTUAL_ENV)${nocolor}%f "
}

username(){
  name=$(whoami)
  [[ $name != "rob" ]] && echo $name
}

hostname2(){
  host=$(hostname -s)
  # [[ $host != water* ]] && echo "@%m:"
  [[ $host != water* ]] && echo "$host"
}

name_and_host() {
  name=$(whoami)
  prefix=""
  suffix=""
  middle=""
  if [[ "$name"  != "rob" ]] ; then
    prefix="${prefix}%F{255}${name}%f"
  fi

  host=$(hostname -s)
  if [[ "$host" != water* && "$host" != Roberts-MacBook-Pro ]] ; then
    suffix="${suffix}%F{255}${host}:"
  fi
  if [[ -n "$prefix" && -n "$prefix" ]] ; then
    middle="@"
  fi
  echo "${prefix}${middle}${suffix}"
}


# set formats
# %b - branchname
# %u - unstagedstr (see below)
# %c - stagedstr (see below)
# %a - action (e.g. rebase-i)
# %m - misc (backend dependent, git shows stash info)
# %R - repository path
# %S - path in the repository
FMT_BRANCH="%b${nocolor}${yellow}%u${nocolor}${lightgreen}%c${nocolor}${lightwhite}%m${nocolor}" # e.g. master¹²
FMT_ACTION=" (${cyan}%a${nocolor}%)"   # e.g. (rebase-i)
FMT_PATH="%R${yellow}/%S"              # e.g. ~/repo/subdir
CHANGE_SYMBOL=' ❚'
CHANGE_SYMBOL=' ★'
# CHANGE_SYMBOL='❱'

zstyle ':vcs_info:*' enable git hg svn
zstyle ':vcs_info:git*:*' get-revision true
zstyle ':vcs_info:git*+set-message:*' hooks git-st git-stash
# check-for-changes can be really slow.
# you should disable it, if you work with large repositories
zstyle ':vcs_info:*:prompt:*' check-for-changes true
zstyle ":vcs_info:*:prompt:*" unstagedstr "${CHANGE_SYMBOL}"  # display ● if there are unstaged changes
zstyle ':vcs_info:*:prompt:*' stagedstr "${CHANGE_SYMBOL}"    # display ● if there are staged changes
zstyle ':vcs_info:*:prompt:*' actionformats " ${FMT_BRANCH}${FMT_ACTION}"
zstyle ':vcs_info:*:prompt:*' formats       " ${FMT_BRANCH}"
zstyle ':vcs_info:*:prompt:*' nvcsformats   "" %m

################### PROMPT ######################
if [[ $UID == 0 ]]; then
    PS1='${white}[${blue}%T ${red}%n ${blue}%1~${white}]%#${nocolor} '
else
    PROMPT='
$(venv)$(name_and_host)${blue}%~${vcs_info_msg_0_}%(1j.${yellow} ⚡${nocolor}.)
%(0?.${lightgreen}.${red})>${nocolor} '
fi
