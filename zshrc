#################################################
#
#    Z Shell configuration file
#
#    version 13
#
#################################################
#
# /etc/zshenv
# Always run for every zsh.
# ~/.zshenv
# Usually run for every zsh.
# /etc/zprofile
# Run for login shells.
# ~/.zprofile
# Run for login shells.
# /etc/zshrc
# Run for interactive shells.
# ~/.zshrc
# Run for interactive shells.
# /etc/zlogin
# Run for login shells.
# ~/.zlogin
# Run for login shells.

# setup autoload path
[[ $fpath = *${HOME}* ]] || fpath=("${HOME}/.zsh/functions" $fpath)

# utf8
# export LC_ALL="en_GB.utf8"
# export LC_ALL=en_GB.UTF-8
export LC="en_GB.utf8"
export LESSCHARSET="utf-8"
# export LANG=en_GB.UTF-8

# less options
export LESS="-FSRX"

# ack options
export ACK_COLOR_MATCH="yellow on_black"

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Projects

# zsh formats `time` in its own way; I'm accustomed to how bash does it
export TIMEFMT=$'\nreal\t%E\nuser\t%U\nsys\t%S'

# print a summary after any long running commands
export REPORTTIME=2

export NODE_PATH="/usr/local/lib/node_modules"
function npm-do { (PATH=$(npm bin):$PATH; eval $@;) }


export FZF_DEFAULT_COMMAND='ag -g ""'

################## ZSH OPTIONS ##################
setopt auto_cd
setopt pushdminus
setopt pushd_ignore_dups
setopt cd_able_vars
setopt extended_glob
setopt correct
setopt check_jobs
setopt multios
setopt no_beep
setopt autopushd
setopt noclobber
setopt no_nomatch
setopt normstarsilent
setopt no_rm_star_silent

# disable xon/xoff
stty -ixon

autoload -U zmv
autoload -U colors && colors
autoload zsh/terminfo

umask 022
#################################################


################## variables ####################

#
# Don't forget that paths are also set in /etc/paths and /etc/paths.d

GOBIN=$HOME/go/bin
CABALPATH=$HOME/.cabal/bin
STACKPATH=$HOME/.local/bin
POSTGRESPATH=/usr/local/opt/postgresql@9.6/bin

typeset -U path
path=($POSTGRESPATH $path $CABALPATH $STACKPATH $GOBIN ~/.bin /usr/texbin .)
cdpath=(~/Projects)

# source some files if they exist on this machine. We need to load compinit
# first so that when we can source file-completion things
#
autoload -Uz compinit && compinit
autoload -U +X bashcompinit && bashcompinit

# [[ -e /usr/local/etc/bash_completion.d/git-completion.bash ]] && source /usr/local/etc/bash_completion.d/git-completion.bash
# virtualenvwrapper
# VIRTUALENVWRAPPER_PYTHON="$(brew --prefix)/bin/python3"
VIRTUALENVWRAPPER_PYTHON=~/.pyenv/versions/3.8.0/bin/python
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
[[ -e /usr/local/bin/virtualenvwrapper.sh ]] && source /usr/local/bin/virtualenvwrapper.sh

# pythonrc
# [[ -e $HOME/.config/python/pythonrc.py ]] && export PYTHONSTARTUP=$HOME/.config/python/pythonrc.py
[[ -e /usr/local/share/zsh/site-functions/_aws ]] && source /usr/local/share/zsh/site-functions/_aws
[[ -e /usr/local/share/zsh/site-functions/_stripe ]] && source /usr/local/share/zsh/site-functions/_stripe
# eval "$(docker-machine env default)"

# ruby stuff
# [[ -e /usr/local/opt/chruby/share/chruby/chruby.sh ]] && source /usr/local/opt/chruby/share/chruby/chruby.sh
# [[ -e /usr/local/opt/chruby/share/chruby/auto.sh ]] && source /usr/local/opt/chruby/share/chruby/auto.sh

# fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

for color in black magenta cyan white red green blue yellow; do
    eval $color='%{$terminfo[bold]$fg[${color}]%}'
    eval light$color='%{$fg[${color}]%}'
done
nocolor="%{$terminfo[sgr0]%}"

export EDITOR="vim"
export CLICOLOR=1

# set some colours for less
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#################################################


################## ALIASES ######################
[[ -e "$HOME/.zsh/aliases.zsh" ]] && source ~/.zsh/aliases.zsh

# since dircolors gets added by the above coreutils alias we cannot set this VAR til here
if [[ -e "$HOME/.dir_colors" ]] then
    eval $(dircolors -b ~/.dir_colors)
else
    LS_COLORS='no=00:fi=00:di=01;32:ln=01;33:pi=40;32:so=01;32:do=01;32:bd=40;33;01:cd=40;33;01:or=40;32;01:ex=01;31:*.tar=00:*.tgz=00:*.arj=00:*.taz=00:*.lzh=00:*.zip=00:*.z=00:*.Z=00:*.gz=00:*.bz2=00:*.deb=01;32:*.rpm=01;32:*.jpg=00;32:*.gif=00;32:*.bmp=00;32:*.ppm=00;32:*.tga=00;32:*.xbm=00;32:*.xpm=00;32:*.tif=00;32:*.png=00;32:*.mov=00;32:*.mp3=00;32:*.mpg=00;32:*.ogm=00;32:*.avi=00;32:*.fli=00;32:*.gl=01;32:*.dl=01;32:'; export LS_COLORS
fi
export ZLS_COLORS="${LS_COLORS}"

if [[ -e "$HOME/.zsh/_zsh-syntax-highlighting-filetypes" ]] then
    # source "$HOME/.zsh/_zsh-syntax-highlighting-filetypes"
fi

#################################################


################# FUNCTIONS ######################
_force_rehash()
{
    (( CURRENT == 1 )) && rehash
    return 1
}
#-----------------------
newpython(){

    if [ ! -n "$1" ]
    then
        echo "Usage: $((basename $0)) FILE"
    else
        echo "#!/usr/bin/env python" > $1
        chmod u+x $1
    fi
}
pg(){
    ps -fe | grep "$1" | grep -v grep
}

mkcd(){
    mkdir -p "$1" && cd "$1"
}
compdef mkcd=mkdir

mkmv(){
    mkdir -p "$1" && mv "$2" "$1"
}

cdp () {
    if [ ! -n "$1" ]
    then
        echo "Usage: cdp modulename"
    else
        cd "$(python -c "import os.path as _, ${1}; \
          print(_.dirname(_.realpath(${1}.__file__[:-1])))"
        )"
    fi
}

fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -r 's/ *[0-9]*\*? *//' | sed -r 's/\\/\\\\/g')
}

fbr() {
  local branches branch
  branches=$(git branch -vv) &&
  branch=$(echo "$branches" | fzf +m) &&
  git checkout $(echo "$branch" | awk '{print $1}' | sed "s/.* //")
}

#################################################


################### HISTORY #####################
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
setopt append_history
setopt no_hist_beep
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt SHARE_HISTORY

HISTFILE=~/.zshhistory
HISTSIZE=10000
SAVEHIST=10000
READNULLCMD=less

autoload -Uz copy-earlier-word
zle -N copy-earlier-word
bindkey "^[m" copy-earlier-word
bindkey '^[[Z' reverse-menu-complete
#################################################


################### PROMPT ######################

autoload -Uz vcs_info

preexec() {
  local a=${${1## *}[(w)1]}  # get the command
  local b=${a##*\/}   # get the command basename
  a="${b}${1#$a}"     # add back the parameters
  a=${a//\%/\%\%}     # escape print specials
  a=$(print -Pn "$a" | tr -d "\t\n\v\f\r")  # remove fancy whitespace
  a=${(V)a//\%/\%\%}  # escape non-visibles and print specials

  case "$TERM" in
    screen|screen.*)
      # See screen(1) "TITLES (naming windows)".
      # "\ek" and "\e\" are the delimiters for screen(1) window titles
      print -Pn "\ek%-3~ $a\e\\" # set screen title.  Fix vim: ".
      print -Pn "\e]2;%-3~ $a\a" # set xterm title, via screen "Operating System Command"
      ;;
    rxvt|rxvt-unicode|xterm|xterm-color|xterm-256color)
      print -Pn "\e]2;%m:%-3~ $a\a"
      ;;
  esac
}

title_precmd() {
  case "$TERM" in
    screen|screen.rxvt)
      print -Pn "\ek%-3~\e\\" # set screen title
      print -Pn "\e]2;%-3~\a" # must (re)set xterm title
      ;;
  esac
}

vcs_precmd(){
    vcs_info 'prompt'
}

precmd_functions+=(title_precmd)
precmd_functions+=(vcs_precmd)

source ~/.zsh/prompt.zsh

#################################################


################### KEYBINDINGS #################
bindkey -e

case $TERM in
    linux)
        bindkey "^[[1~" beginning-of-line       # Home
        bindkey "^[[4~" end-of-line             # End
        bindkey "^[[3~" delete-char             # Del
        bindkey "^[[2~" overwrite-mode          # Insert
        bindkey "^[[5~" history-search-backward # PgUp
        bindkey "^[[6~" history-search-forward  # PgDn
        ;;

    xterm)
        bindkey "^[OH"  beginning-of-line       # Home
        bindkey "^[OF"  end-of-line             # End
        bindkey "^[[3~" delete-char             # Del
        bindkey "^[[2~" overwrite-mode          # Insert
        # bindkey "^[[5~" history-search-backward # PgUp
        # bindkey "^[[6~" history-search-forward  # PgDn
        bindkey "^[[A" history-beginning-search-backward # Up
        bindkey "^[[B" history-beginning-search-forward  # Dn
        ;;
    *rxvt)
        bindkey "^[[7~"  beginning-of-line       # Home
        bindkey "^[[8~"  end-of-line             # End
        bindkey "^[[3~" delete-char             # Del
        bindkey "^[[2~" overwrite-mode          # Insert
        # bindkey "^[[5~" history-search-backward # PgUp
        # bindkey "^[[6~" history-search-forward  # PgDn
        bindkey "^[[A" history-beginning-search-backward # Up
        bindkey "^[[B" history-beginning-search-forward  # Dn
        ;;

    cons25l2)
        bindkey "^[[H"  beginning-of-line       # Home
        bindkey "^[[F"  end-of-line             # End
        bindkey "^?"    delete-char             # Del
        bindkey "^[[L"  overwrite-mode          # Insert
        bindkey "^[[I"  history-search-backward # PgUp
        bindkey "^[[G"  history-search-forward  # PgDn
        ;;
    *)
        bindkey "\e[A" history-beginning-search-backward
        bindkey "\e[B" history-beginning-search-forward
        bindkey "^[[3~" delete-char             # Del
        bindkey "^[[1~" beginning-of-line       # Home
        bindkey "^[[4~" end-of-line             # End
        bindkey '^P' up-line-or-beginning-search
        bindkey '^N' down-line-or-beginning-search
        # bindkey '^R' history-incremental-pattern-search-backward
        ;;

esac
#################################################


################## COMPLETION ###################
setopt always_to_end
setopt complete_in_word
setopt list_packed
# setopt no_menu_complete
setopt glob_complete

# auto-escape urls
autoload -U url-quote-magic
zle -N self-insert url-quote-magic
autoload -Uz bracketed-paste-url-magic
zle -N bracketed-paste bracketed-paste-url-magic

# autoload -Uz compinit && compinit
# autoload -U +X bashcompinit && bashcompinit
zmodload -i zsh/complist

# SSH Completion
zstyle ':completion:*:scp:*' tag-order \
   files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order \
   files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
   users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order \
   hosts-domain hosts-host users hosts-ipaddr
zstyle '*' single-ignored show


# cache the completion
zstyle ':completion::complete:*' use-cache 1
#################################################
# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _force_rehash
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents pwd
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*:builtins' list-colors '=*=1;38;5;142'
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]} r:|[._-]=* r:|=*'
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' menu yes select
zstyle :compinstall filename '$HOME/.zshrc'

# End of lines added by compinstall

# colour completion for kill
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

# extras for the kill menu
zstyle ':completion:*:processes' command 'ps -U $(whoami) | sed "/ps/d"'
zstyle ':completion:*:processes' insert-ids menu yes select

# no completion for commands not installed
zstyle ':completion:*:functions' ignored-patterns '_*'

# Remove uninteresting users
zstyle ':completion:*:*:*:users' ignored-patterns \
   avahi gdm bin daemon dbus ftp hal http mail mpd mysql nobody ntp \
   policykit postgres '_*'

# do not complete usernames
# zstyle ':completion:*' users off

# Completion of hostnames, pulled out of ~/.ssh/config.
if [ -f ~/.ssh/config ]; then
  local _myhosts
  _myhosts=( $(cat ~/.ssh/config|grep '^H'|cut -d ' ' -f 2) )
  zstyle ':completion:*' hosts ${_myhosts[1,-2]} # Slices off the last entry
fi

# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.pyc' '*?.swp' \

# when completing cd commands, separate candidates into groups
zstyle ':completion:*:cd:*' group-name ''
zstyle ':completion:*:cd:*descriptions' format %K{black}%F{white}%d%f%k

# start tmux
if type -p tmux >/dev/null 2>&1; then
    WHOAMI=$(whoami)
    if [[ -z "$TMUX" ]] ; then
        tmux new-session -A -s $WHOAMI
    fi
fi

if [ -f ~/.ssh-agent ]; then
    source ~/.ssh-agent
fi

# if [ -z "$SSH_AUTH_SOCK" ] || [ ! -w "$SSH_AUTH_SOCK" ]; then
#     if read -q '?Start ssh-agent? (y/n) '; then
#         ssh-agent -s >! ~/.ssh-agent && \
#             source ~/.ssh-agent && \
#             ssh-add -K
#     fi
# fi

# setup direnv
eval "$(direnv hook zsh)"

# The next line updates PATH for the Google Cloud SDK.
# if [ -f '/Users/rob/Projects/zerodeposit/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/rob/Projects/zerodeposit/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
# if [ -f '/Users/rob/Projects/zerodeposit/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/rob/Projects/zerodeposit/google-cloud-sdk/completion.zsh.inc'; fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

[ -f "${GHCUP_INSTALL_BASE_PREFIX:=$HOME}/.ghcup/env" ] && source "${GHCUP_INSTALL_BASE_PREFIX:=$HOME}/.ghcup/env"
